import { prisma } from '.'

import type { Mission } from '@prisma/client'

const DEFAULT_TASKS = [
  {
    year: 2022,
    name: 'Name',
    field: 'Field',
    responsibility: 'Resp',
    dateToFinish: new Date(),
    comments: 'Comments',
    officialComments: 'Official Comments',
    type: 'IMPLEMENT',
    status: 'OPEN',
  },
  {
    year: 2022,
    name: 'Name 2',
    field: 'Field',
    responsibility: 'Resp',
    dateToFinish: new Date(Date.now() + '1000000'),
    comments: 'Comments 2',
    officialComments: 'Official Comments 2',
    type: 'SCRAP',
    status: 'FROZEN',
  },
  {
    year: 2023,
    name: 'Name 3',
    field: 'Field',
    responsibility: 'Resp 2',
    dateToFinish: new Date(Date.now() + '1000000'),
    comments: 'Comments 3',
    officialComments: 'Official Comments 3',
    type: 'SCRAP',
    status: 'FROZEN',
  },
] as Array<Mission>

;(async () => {
  try {
    await prisma.mission.deleteMany()
    await Promise.all(
      DEFAULT_TASKS.map((task) =>
        prisma.mission.createMany({
          data: { ...task },
        })
      )
    )
    console.log('Seeding Finished')
  } catch (error) {
    console.error(error)
    process.exit(1)
  } finally {
    await prisma.$disconnect()
  }
})()
