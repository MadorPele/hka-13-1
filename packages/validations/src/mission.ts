import { z } from 'zod'
import { type Mission } from 'database'
import { ZodObject, type NonEmptyArray } from './utils'

const missionShape = {
  id: z.number().min(1),
  year: z.number(),
  name: z.string().min(1),
  field: z.string().min(1),
  responsibility: z.string().min(1),
  dateToFinish: z.date().nullable(),
  comments: z.string().min(1).nullable(),
  officialComments: z.string().min(1).nullable(),
  dateFinished: z.date().nullable(),
  type: z.enum(['IMPLEMENT', 'SCRAP']),
  status: z.enum(['OPEN', 'CLOSED', 'FINISHED', 'CANCELED', 'FROZEN']),
}

export const zodMission = ZodObject<Mission>().shape(missionShape)

export const zodMissionField = z.enum(
  Object.keys(missionShape) as NonEmptyArray<keyof typeof missionShape>
)

export const zodMissionNoId = zodMission.omit({
  id: true,
})
