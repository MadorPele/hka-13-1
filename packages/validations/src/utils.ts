import { z } from 'zod'

export type NonEmptyArray<T> = [T, ...T[]]

type ZodShape<Model> = {
  [key in keyof Model]-?: undefined extends Model[key]
    ? null extends Model[key]
      ? z.ZodNullableType<z.ZodOptionalType<z.ZodType<Model[key]>>>
      : z.ZodOptionalType<z.ZodType<Model[key]>>
    : null extends Model[key]
    ? z.ZodNullableType<z.ZodType<Model[key]>>
    : z.ZodType<Model[key]>
}

export function ZodObject<Model = never>() {
  return {
    shape: <
      Schema extends ZodShape<Model> & {
        [unknownKey in Exclude<keyof Schema, keyof Model>]: never
      }
    >(
      schema: Schema
    ) => z.object(schema),
  }
}
