// init trpc
import { createTRPCReact } from '@trpc/react-query'
import type { AppRouter } from 'api'
export default createTRPCReact<AppRouter>()
