import trpc from '.'
import type { AppRouter } from 'api'
import type { inferProcedureInput } from '@trpc/server'

export function useMissions(
  input: inferProcedureInput<AppRouter['mission']['getAll']>
) {
  return trpc.mission.getAll.useQuery(input)
}

export function useMission(
  input: inferProcedureInput<AppRouter['mission']['getOne']>
) {
  return trpc.mission.getOne.useQuery(input)
}

export function useAddMission() {
  const utils = trpc.useContext()
  return trpc.mission.addOne.useMutation({
    onSuccess: () => {
      console.log('משימה נוספה בהצלחה')
      utils.mission.getAll.invalidate()
    },
    onError: () => {
      console.error('שגיאה')
    },
  })
}

export function useEditMission() {
  const utils = trpc.useContext()
  return trpc.mission.editOne.useMutation({
    onSuccess: (mission) => {
      console.log('משימה נערכה בהצלחה')
      utils.mission.getAll.invalidate()
      utils.mission.getOne.invalidate({ id: mission.id })
    },
    onError: () => {
      console.error('שגיאה')
    },
  })
}

export function useDeleteMission() {
  const utils = trpc.useContext()
  return trpc.mission.deleteOne.useMutation({
    onSuccess: () => {
      console.log('משימה נמחקה בהצלחה')
      utils.mission.getAll.invalidate()
    },
    onError: () => {
      console.error('שגיאה')
    },
  })
}
