import { useEffect } from 'react'
import { useAddMission, useMissions, useEditMission } from '~/lib/trpc/hooks'
import TrpcProvider from '~/lib/trpc/Provider'

function TrpcTest() {
  const { data, isLoading } = useMissions({ select: ['id', 'name'] })
  const { mutate: editMission } = useEditMission()
  const { mutate: addMission } = useAddMission()

  useEffect(() => {
    editMission({
      id: 1,
      data: {
        name: 'name 1',
      },
    })
  }, [])

  if (isLoading) return <h3>loading...</h3>
  return <pre>{JSON.stringify(data, null, 2)}</pre>
}

function App() {
  return (
    <TrpcProvider>
      <div className="App">
        <TrpcTest />
      </div>
    </TrpcProvider>
  )
}

export default App
