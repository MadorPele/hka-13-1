import { z } from 'zod'
import * as dotenv from 'dotenv'
dotenv.config()

const envSchema = z.object({
  DATABASE_URL: z.string().url(),
  NODE_ENV: z.enum(['development', 'test', 'production']),
})

const envParsed = envSchema.safeParse(process.env)

if (!envParsed.success) {
  console.error(
    '❌ Invalid environment variables:',
    JSON.stringify(envParsed.error.format(), null, 4)
  )
  process.exit(1)
}

export const env = envParsed.data
