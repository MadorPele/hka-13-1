import { initTRPC } from '@trpc/server'
import superjson from 'superjson'
import { Context } from './context'

export const t = initTRPC.context<Context>().create({
  transformer: superjson,
})

export const middleware = t.middleware
export const router = t.router
export const publicProcedure = t.procedure

import { missionRouter } from '../routers/mission'

export const appRouter = t.router({
  mission: missionRouter,
})

export type AppRouter = typeof appRouter
