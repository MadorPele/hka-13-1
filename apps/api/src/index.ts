import { env } from './env'
import express from 'express'
import * as trpcExpress from '@trpc/server/adapters/express'
import { expressHandler } from 'trpc-playground/handlers/express'
import { appRouter } from './trpc'
import { createContext } from './trpc/context'
import cors from 'cors'

const runApp = async () => {
  const PORT = Number(process.env.PORT) || 4000
  const app = express()
  app.use(cors())

  const trpcApiEndpoint = '/api/trpc'
  const playgroundEndpoint = '/api/trpc-playground'

  app.use(
    trpcApiEndpoint,
    trpcExpress.createExpressMiddleware({
      router: appRouter,
      createContext,
    })
  )

  app.use(
    playgroundEndpoint,
    await expressHandler({
      trpcApiEndpoint,
      playgroundEndpoint,
      router: appRouter,
      request: {
        superjson: true,
      },
    })
  )

  app.listen(PORT, () => {
    if (env.NODE_ENV === 'development') {
      console.log(
        'tRPC Server listening on: \x1b[34m%s\x1b[0m',
        `http://localhost:${PORT}${trpcApiEndpoint}`
      )
      console.log(
        'tRPC playground: \x1b[35m%s\x1b[0m',
        `http://localhost:${PORT}${playgroundEndpoint}`
      )
    } else {
      console.log(`tRPC Server listening on port ${PORT} on ${trpcApiEndpoint}`)
      console.log(
        `tRPC Playground listening on port ${PORT} on ${playgroundEndpoint}`
      )
    }
  })
}
runApp()

// trpc type definitions
export { type AppRouter } from './trpc'
