import { prisma } from 'database'
import { z } from 'zod'
import { zodMissionNoId } from 'validations'
import { publicProcedure } from '../../trpc'

export default publicProcedure
  .input(
    // input
    z.object({
      data: zodMissionNoId,
    })
  )
  .mutation(async ({ input }) => {
    return await prisma.mission.create({
      data: input.data,
    })
  })
