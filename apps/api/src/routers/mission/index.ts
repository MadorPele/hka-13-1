import { router } from '../../trpc'

// procedures
import getAll from './get-all'
import getOne from './get-one'
import addOne from './add-one'
import editOne from './edit-one'
import deleteOne from './delete-one'

export const missionRouter = router({
  getAll,
  getOne,
  addOne,
  editOne,
  deleteOne,
})
