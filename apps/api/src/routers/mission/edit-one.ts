import { prisma } from 'database'
import { z } from 'zod'
import { zodMissionNoId } from 'validations'
import { publicProcedure } from '../../trpc'

export default publicProcedure
  .input(
    // input
    z.object({
      id: z.number(),
      data: zodMissionNoId.partial(),
    })
  )
  .mutation(async ({ input }) => {
    return await prisma.mission.update({
      where: { id: input.id },
      data: input.data,
    })
  })
