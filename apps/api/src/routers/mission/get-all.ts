import { Mission, prisma } from 'database'
import { z } from 'zod'
import { zodMission, zodMissionField } from 'validations'
import { publicProcedure } from '../../trpc'

export default publicProcedure
  .input(
    // input
    z
      .object({
        where: zodMission.partial(),
        select: zodMissionField
          .array()
          .transform((arr) => (arr.length ? arr : undefined)),
      })
      .partial()
      .optional()
  )
  .query(async ({ input }) => {
    return await prisma.mission.findMany({
      where: input?.where,
      select: input?.select?.reduce(
        (v, k) => (
          ((
            v as {
              [key in keyof Mission]: boolean
            }
          )[k] = true),
          v
        ),
        {}
      ),
      orderBy: {
        id: 'asc',
      },
    })
  })
