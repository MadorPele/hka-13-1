import { prisma } from 'database'
import { z } from 'zod'
import { publicProcedure } from '../../trpc'

export default publicProcedure
  .input(
    // input
    z.object({
      id: z.number(),
    })
  )
  .mutation(async ({ input }) => {
    return await prisma.mission.delete({
      where: { id: input.id },
    })
  })
