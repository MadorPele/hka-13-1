import { prisma } from 'database'
import { z } from 'zod'
import { TRPCError } from '@trpc/server'
import { publicProcedure } from '../../trpc'

export default publicProcedure
  .input(
    // input
    z.object({
      id: z.number(),
    })
  )
  .query(async ({ input }) => {
    const result = await prisma.mission.findUnique({
      where: {
        id: input.id,
      },
    })
    if (!result)
      throw new TRPCError({
        code: 'NOT_FOUND',
        message: 'Mission not found',
      })
    return result
  })
